module.exports = {
    module: {
        rules: [
            {
                test: /\.scl$/i,
                use: 'raw-loader',
            },
            {
                test: /\.txt$/i,
                use: 'raw-loader',
            },
        ],
    },
};

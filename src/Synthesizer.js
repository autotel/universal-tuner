import Model from "./scaffolding/Model";

class Voice {
    constructor({audioContext}){
        const releaseSeconds = 2;
        const ampPeak = 0.25;
        this.inUse=false;
        let oscillator = audioContext.createOscillator();
        oscillator.start();
        let amp = audioContext.createGain();
        amp.gain.value=0;

        oscillator.connect(amp)
        amp.connect(audioContext.destination);

        this.startTone=(frequency)=>{
            this.inUse=true;
            oscillator.frequency.value=frequency;
            oscillator.frequency.setValueAtTime(frequency, audioContext.currentTime); 
            
            amp.gain.linearRampToValueAtTime(ampPeak,audioContext.currentTime+0.1);
        }
        this.stopTone=(frequency)=>{
            setTimeout(()=>{
                this.inUse=false;
            },releaseSeconds * 1000 + 5);
            amp.gain.linearRampToValueAtTime(0,audioContext.currentTime + releaseSeconds);
        }
    }
}

class Synthesizer {
    constructor({audioContext}){
        const settings = {
            tonePlaying:false,
        }
        /** @type {Array<Voice>} */
        const voices=[
        ];
        /** @type {Object<string,Voice>} */
        let frequencyVoices = {};
        /** @returns {Voice} */
        const findVoice=function(){
            let found = false;
            for(let voice of voices){
                if(!voice.inUse) return voice;
            }
            let newVoice = new Voice({audioContext});
            voices.push(newVoice);
            return newVoice;
        }

        this.startTone=(frequency)=>{
            frequencyVoices[frequency+""] = findVoice();
            frequencyVoices[frequency+""].startTone(frequency);
        };
        this.stopTone=(frequency)=>{
            try{
                frequencyVoices[frequency+""].stopTone();
                delete frequencyVoices[frequency+""];
            }catch(e){
                this.stopAll();
            }

        };
        this.stopAll=()=>{
            for(let voice of voices){
                voice.stopTone();
            }
        }
    }
}
export default Synthesizer;
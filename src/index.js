import AudioContextGetter from "./model-dom/Audio/AudioContextGetter";
import DisplayFft from "./DisplayFft";
import AudioCapture from "./AudioCapture";
import DisplayFrequencyList from "./DisplayFrequencyList";
import Synthesizer from "./Synthesizer";
import { Div, P, H1 } from "./model-dom/GuiComponents/DOMElements";
import FrequencyList from "./FrequencyList";
import ScalaTextLoader from "./ScalaTextLoader";
import Draggable from "./model-dom/Interactive/Draggable";
import KeyboardPiano from "./KeyboardPiano";
import PresetSelector from "./PresetsSelector";


let getter = new AudioContextGetter();

let mainElement = new Div();
let title = new H1({ text: "click to start!" });
document.addEventListener("mousedown", () => {
    title.domElement.classList.add("hidden");
});
mainElement.add(title);
document.body.appendChild(mainElement.domElement);

Draggable.setCanvas(document);

getter.get().then((audioContext) => {
    let frequencyList = new FrequencyList();
    let keyboard = new KeyboardPiano({ frequencyList });
    let audioCapture = new AudioCapture({ audioContext });
    let fftDisp = new DisplayFft({
        audioCapture, audioContext, frequencyList
    });
    let synthesizer = new Synthesizer({ audioContext });
    let frequencyListdisplay = new DisplayFrequencyList({ synthesizer, frequencyList, keyboard });
    let scalaLoader = new ScalaTextLoader({ frequencyList });
    let presetSelector = new PresetSelector({frequencyList});

    mainElement.add(
        fftDisp,
        frequencyListdisplay, 
        presetSelector,
        scalaLoader,
    );
    frequencyList.setFrequencies([
        60, 65.45454545454545, 70, 76.36363636363636, 80, 90, 94.28571428571428,102.85714285714285, 110, 
        120, 130.9090909090909, 140, 152.72727272727272, 160, 180, 188.57142857142856, 205.7142857142857, 220, 
        240, 261.8181818181818, 280, 305.45454545454544, 320, 360, 377.1428571428571, 411.4285714285714, 440, 
        480, 523.6363636363636, 560, 610.9090909090909, 640, 720, 754.2857142857142, 822.8571428571428, 880, 
        960, 1047.2727272727273, 1120, 1221.8181818181818, 1280, 1440, 1508.5714285714284, 1645.7142857142856, 1760, 
        1920, 2094.5454545454545, 2240, 2443.6363636363635, 2560, 
        2880, 3017.142857142857, 3291.428571428571, 3520, 3840, 4189.090909090909, 4480, 4887.272727272727, 5120, 
        5760, 6034.285714285714, 6582.857142857142, 7040, 7680, 8378.181818181818, 8960, 9774.545454545454, 10240, 
        11520, 12068.571428571428, 13165.714285714284, 14080, 15360, 16756.363636363636, 17920, 19549.090909090908, 20480, 
        22050
    ]);

    const keyboardHint = new P({ class:"hint-keyboard", text: "⌨⇨🎜" });
    mainElement.add(keyboardHint);
    
    keyboard.onUpdate((changes)=>{
        console.log(changes);
        if(keyboard.settings.willPlay){
            keyboardHint.domElement.classList.remove("hidden");
        }else{
            keyboardHint.domElement.classList.add("hidden");
        }
    });
});
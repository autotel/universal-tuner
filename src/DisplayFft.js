/**
 * displays an fft and numbers indicating the frequencies, plus a line
 * at the sleceted frequency on DisplayFrequncy kist
 * 
 */

import { Div, P } from "./model-dom/GuiComponents/DOMElements";
import { SVG, Path, Line, BoxedText, Circle } from "./model-dom/GuiComponents/SVGElements";
import AudioCapture from "./AudioCapture";
import FrequencyList from "./FrequencyList";
import Draggable from "./model-dom/Interactive/Draggable";
import Model from "./scaffolding/Model";
import mobileLog from "./model-dom/utils/mobile-log";

class FrequencyLine extends SVG {
    constructor() {
        super({});
        this.domElement.classList.add("frequency-line");
        const line = new Line();
        const label = new BoxedText();
        this.isShowing = true;
        this.isHighlighted = false;
        this.frequency=0;
        this.add(line, label);
        this.hide = () => {
            if (!this.isShowing) return;
            this.domElement.classList.add("hidden");
            this.isShowing = false;
        }
        this.show = () => {
            if (this.isShowing) return;
            this.domElement.classList.remove("hidden");
            this.isShowing = true;
        }
        this.highlight = () => {
            if(this.isHighlighted) return;
            this.domElement.classList.add("highlight");
            this.isHighlighted=true;
        }
        this.removeHighlight = () => {
            if(!this.isHighlighted) return;
            this.domElement.classList.remove("highlight");
            this.isHighlighted=false;
        }
        this.set = (settings) => {
            this.show;
            const {
                height, frequency, top, left
            } = settings;

            line.attributes.x1 = left;
            line.attributes.y1 = top;
            line.attributes.x2 = left;
            line.attributes.y2 = top + height;
            label.attributes.text = (Math.round(frequency*100) / 100)+"hz";
            label.attributes.x = left;
            label.attributes.y = height + top + 10;
            label.attributes.transform = `rotate(30,${
                label.attributes.x
                },${
                label.attributes.y
                })`;
            this.frequency=frequency;
            line.update();
            label.update();

        }
    }
}
class ScaleFrequencyLine extends FrequencyLine {
    constructor() {
        super();
        this.domElement.classList.add("scale-frequency-line");
    }
}
class PeakIndicator extends SVG {
    constructor(options) {
        super(options);
        const circle = new Circle({ r: 10 });
        const label = new BoxedText();
        this.domElement.classList.add("peak-indicator");
        this.add(circle, label);
        this.hide = () => {
            this.domElement.classList.add("hidden");
        }
        this.set = (settings) => {
            this.domElement.classList.remove("hidden");
            const {
                x, y, frequency,
            } = settings;

            circle.attributes.cx = x;
            circle.attributes.cy = y;

            label.attributes.text = Math.round(frequency*100)/100 + "Hz";
            label.attributes.x = x + 16;
            label.attributes.y = y;

            circle.update();
            label.update();

        }
    }
}

class FftView extends Model {

    /** 
     * @param {Object} settings
     * @param {number} settings.frequencyBinCount
     * @param {number} settings.sampleRate
     * @param {number} settings.binMin
     * @param {number} settings.binMax
     * @param {number} settings.levelMin
     * @param {number} settings.levelMax
     * @param {number} settings.viewWidth
     * @param {number} settings.viewHeight
     * 
     */
    constructor(settings) {

        super(settings);

        /**
         * https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode/getFloatFrequencyData
         * 
         * Each item in the array represents the decibel value for a specific 
         * frequency. The frequencies are spread linearly from 0 to 1/2 of 
         * the sample rate. For example, for a 48000 Hz sample rate, the last 
         * item of the array will represent the decibel value for 24000 Hz.
         * */
        this.fftBinToFrequency = (binNo) => {
            const { frequencyBinCount, sampleRate } = settings;
            const binAmt = frequencyBinCount;
            return (sampleRate / 2) * binNo / binAmt;
        }
        this.frequencyToFftBin = (frequency) => {
            const { frequencyBinCount, sampleRate } = settings;
            const binAmt = frequencyBinCount;
            return frequency * binAmt / (sampleRate / 2);
        }
        this.xToFftBin = (x) => {
            const {
                binMin,
                binMax,
                viewWidth
            } = settings;
            const fftBinsRange = binMax - binMin;
            return Math.ceil(fftBinsRange * x / viewWidth + binMin);
        }
        this.fftBinTox = (fftBin) => {
            const { binMax, binMin, viewWidth } = settings;
            const fftBinsRange = binMax - binMin;
            return (fftBin - binMin) * viewWidth / fftBinsRange;
        }
        this.xToFrequency = (x) => {
            return this.fftBinToFrequency(
                this.xToFftBin(x)
            );
        }
        this.frequencyToX = (frequency) => {
            return this.fftBinTox(
                this.frequencyToFftBin(frequency)
            );
        }
        this.fftLevelToY = (level) => {
            const {
                levelMin,
                levelMax,
                viewHeight
            } = settings;
            let offset = levelMin;
            let range = levelMax - levelMin;
            if (range == 0) return 0;
            let multiplier = 1 / range;
            return viewHeight - (level - offset) * multiplier * viewHeight
        }

    }
}

class DisplayFft extends Div {
    /** 
     * @param {Object} things
     * @param {AudioCapture} things.audioCapture
     * @param {AudioContext} things.audioContext
     * @param {FrequencyList} things.frequencyList
     */
    constructor({ audioCapture, audioContext, frequencyList }) {
        super({});

        const fftLevelRange = {
            min: 0,
            max: 0,
        }
        const svgSize = {
            width: 500,
            height: 140,
        }

        const view = new FftView({
            viewWidth: svgSize.width,
            viewHeight: 100,
            binMin: 10,
            binMax: 100,
            sampleRate: audioContext.sampleRate,
            frequencyBinCount: 1,
            levelMin: 0,
            levelMax: 0,
        });


        //create HTML elements
        let text = new P({ text: "(drag to pan)", style:"right:0px;" });
        text.domElement.classList.add("hint");
        let svgDisplay = new SVG({ viewBox: `0 0 ${svgSize.width} ${svgSize.height}` });
        this.add(text, svgDisplay);

        let svgFront = new SVG();
        let svgMiddle = new SVG();
        let svgBack = new SVG();

        svgDisplay.add(svgBack, svgMiddle, svgFront);

        //create SVG elements
        let svgPath = new Path();
        let svgDeltaPath = new Path();

        svgMiddle.add(svgPath, svgDeltaPath);
        svgPath.domElement.classList.add("fft");
        svgDeltaPath.domElement.classList.add("fft");
        svgDeltaPath.domElement.classList.add("delta");
        /** @type {Array<FrequencyLine>} */
        const frequencyLines = [];
        const drawFrequencyLine = (number, properties) => {
            if (!frequencyLines[number]) {
                frequencyLines[number] = new FrequencyLine();
                svgBack.add(frequencyLines[number]);
            }
            frequencyLines[number].set(properties);
        }
        const hideFrequencyLines = (from, to = frequencyLines.length) => {
            for (let index = from; index < to; index++) frequencyLines[index].hide();
        }

        const scaleFequencyLines = [];
        const drawScaleFrequencyLine = (number, properties) => {
            if (!scaleFequencyLines[number]) {
                scaleFequencyLines[number] = new ScaleFrequencyLine();
                svgMiddle.add(scaleFequencyLines[number]);
            }
            scaleFequencyLines[number].set(properties);
        }
        const hideScaleFrequencyLines = (from, to = scaleFequencyLines.length) => {
            for (let index = from; index < to; index++) scaleFequencyLines[index].hide();
        }

        const peakIndicators = [];
        const drawpeakIndicator = (number, properties) => {
            if (!peakIndicators[number]) {
                peakIndicators[number] = new PeakIndicator();
                svgFront.add(peakIndicators[number]);
            }
            peakIndicators[number].set(properties);
        }
        /** @type {Array<number>} */
        let fftArray = [];
        let fftSlowerArray = [];
        let fftDeltaArray = [];
        const analyser = audioContext.createAnalyser();
        /**
         * @typedef {Object} Peak
         * @property {number} Peak.bin
         * @property {number} Peak.level
         * @property {number} [Peak.frequency]
         */
        /**
         * @type {Array<Peak>} 
         */
        const peaks = [];

        /** calculate peak frequency based on weighted averages */
        /** @param {Peak} peak */
        const calculatePeakFrequency = (peak) => {
            peak.frequency = 0;
            let sumOfLevels = 0;
            let peaksAround = 1;

            for (
                let binNumer = peak.bin - peaksAround; 
                binNumer <= peak.bin + peaksAround; 
                binNumer++
            ) {
                sumOfLevels += fftArray[binNumer];
            }

            let sumOfWeights = 0;

            for (
                let binNumer = peak.bin - peaksAround; 
                binNumer <= peak.bin + peaksAround; 
                binNumer++
            ) {
                let binFq = view.fftBinToFrequency(binNumer);
                let weight = fftArray[binNumer] / sumOfLevels;
                sumOfWeights += weight;
                peak.frequency += binFq * weight;
            }
            if(isNaN(peak.frequency)){
                console.warn({
                    sumOfWeights,sumOfLevels,
                    peakbin:peak.bin,
                    fft1:fftArray[peak.bin-1] / sumOfLevels,
                    fft2:fftArray[peak.bin] / sumOfLevels,
                    fft3:fftArray[peak.bin+1] / sumOfLevels,
                });
                peak.frequency=0;
            }

            return peak.frequency;
        }

        const round2 = (num) => {
            return Math.round(num * 100) / 100;
        }

        const updateFFTDrawing = () => {
            const length = fftArray.length;

            let pathStr = "M0 0";
            let deltaPathStr = pathStr;
            let maxval = 0;
            let minval = 0;
            for (let x = 0; x < view.settings.viewWidth; x++) {
                const binNum = view.xToFftBin(x);
                const y = view.fftLevelToY(fftArray[binNum]);
                const dx = x
                const dy = view.fftLevelToY(fftDeltaArray[binNum]) + view.settings.viewHeight / 2;

                if (fftArray[binNum] > maxval) maxval = fftArray[binNum];
                if (fftArray[binNum] < minval) minval = fftArray[binNum];

                pathStr += ` L${x} ${y} `
                deltaPathStr += ` L${dx} ${dy} `
            }


            view.set({
                levelMax: maxval * 0.1 + view.settings.levelMax * 0.9,
                levelMin: minval * 0.1 + view.settings.levelMin * 0.9,
            });

            svgPath.set("d", pathStr);
            svgDeltaPath.set("d", deltaPathStr);
        }

        const updateLines = () => {
            const fftStart = view.settings.binMin;
            const fftEnd = view.settings.binMax;
            const fftBinsRange = fftEnd - fftStart;

            const length = fftArray.length;
            const amountOfLines = 10;
            const pixelsPerLine = view.settings.viewWidth / amountOfLines;

            for (let lineNumber = 0; lineNumber < amountOfLines; lineNumber++) {
                let x = lineNumber * pixelsPerLine;
                drawFrequencyLine(lineNumber, {
                    height: view.settings.viewHeight,
                    frequency: view.xToFrequency(x),
                    top: 0,
                    left: x
                });
            }
            hideFrequencyLines(amountOfLines);

            frequencyList.settings.frequencies.forEach((frequency, index) => {
                let x = view.frequencyToX(frequency);
                drawScaleFrequencyLine(index, {
                    height: view.settings.viewHeight,
                    frequency: frequency,
                    top: 0,
                    left: x
                });
            });
            hideScaleFrequencyLines(frequencyList.settings.frequencies.length);

        }

        const updateSelectedFrequency = (selectedFreq) => {
            scaleFequencyLines.forEach((frequencyLine, index) => {
                // mobileLog(frequencyLine.frequency-selectedFreq);
                if(frequencyLine.frequency == selectedFreq ){
                    frequencyLine.highlight();
                }else{
                    frequencyLine.removeHighlight();
                }
            });
        }

        const findPeaksIn = (array, binStart) => {
            //TODO: this will not work for more than one peak
            peaks[0] = {
                level: -1000,
                bin: 0,
            }
            binStart=Math.floor(binStart);
            array.forEach((level, index) => {
                if (level > peaks[0].level) {
                    peaks[0].level = level;
                    peaks[0].bin = index + binStart;
                }
            });
        }

        const updatePeakIndicators = () => {
            peaks.forEach((peak, index) => {
                let frequency=calculatePeakFrequency(peak);
                drawpeakIndicator(index, {
                    x: view.fftBinTox(peak.bin),
                    y: view.fftLevelToY(peak.level),
                    frequency,
                });
            });
        }

        const draw = () => {
            const dataArray = new Float32Array(analyser.frequencyBinCount);
            analyser.getFloatFrequencyData(dataArray);
            //get array, saturating so we don't get infinite
            fftArray = Array.from(dataArray).map((val) => {
                if (val == -Infinity) return -100;
                return val;
            });
            fftArray.forEach((value, index) => {
                if (!fftSlowerArray[index]) fftSlowerArray[index] = 0;
                fftSlowerArray[index] = fftSlowerArray[index] * 0.99
                    + value * 0.01;
                fftDeltaArray[index] = value - fftSlowerArray[index];
            });

            findPeaksIn(fftArray.slice(
                view.settings.binMin,
                view.settings.binMax
            ), view.settings.binMin);

            updateFFTDrawing();
            updatePeakIndicators();
        }
        //events

        let draggable = new Draggable(svgDisplay.domElement);
        let hDragBinStart = 0;
        draggable.dragCallback = (mouse) => {
            const mouseBin = Math.round(view.xToFftBin(mouse.x));
            const deltaBin = hDragBinStart - mouseBin;

            const binRange = view.settings.binMax - view.settings.binMin;

            const changes = {}
            changes.binMin = view.settings.binMin + deltaBin;
            if (changes.binMin < 0) changes.binMin = 0;
            changes.binMax = changes.binMin + binRange;

            // mobileLog({deltaBin,mouseBin,changes,hDragBinStart});
            view.set(changes);
        }

        draggable.dragStartCallback = (mouse) => {
            text.domElement.classList.add("hidden");
            hDragBinStart = Math.round(view.xToFftBin(mouse.x));
        }
        draggable.dragEndCallback = (mouse) => {
        }

        frequencyList.onUpdate((frequencyListChanges) => {
            if (frequencyListChanges.frequencies) {
                updateLines();
            }
            if (frequencyListChanges.selected!==undefined) {
                if(frequencyListChanges.selected===false){

                }else{
                    //mobileLog(frequencyListChanges.selected);
                    const viewChanges = {};
                    const binRange = view.settings.binMax - view.settings.binMin;
                    const halfSpectrum = binRange / 2;
                    viewChanges.binMin = view.frequencyToFftBin(frequencyListChanges.selected) - halfSpectrum;
                    if (viewChanges.binMin < 0) viewChanges.binMin = 0;
                    viewChanges.binMax = viewChanges.binMin + binRange;
                    view.set(viewChanges);
                }
                updateSelectedFrequency(frequencyListChanges.selected);
            }
        });

        view.onUpdate((changes) => {
            /*
            changes.frequencyBinCount
            changes.sampleRate
            changes.binMin
            changes.binMax
            changes.levelMin
            changes.levelMax
            changes.viewWidth
            changes.viewHeight
            */
            if (
                changes.frequencyBinCount
                || changes.sampleRate
                || changes.binMin
                || changes.binMax
                || changes.viewWidth
                || changes.viewHeight
            ) {
                updateLines();
            }
        });

        //start 
        audioCapture.onReady(() => {
            updateFFTDrawing();

            //44100 / 1024 = 43hz
            analyser.fftSize = 2048 * 8;


            if (audioCapture.audioSource) {
                audioCapture.audioSource.connect(analyser);
            } else {
                throw new Error(
                    "unexpectedly falsey audiosource "
                    + audioCapture.audioSource
                );
            }

            const d = () => {
                draw();
                requestAnimationFrame(d);
            };
            d();

            view.set({
                frequencyBinCount: analyser.frequencyBinCount
            });

            updateLines();
            // setInterval(d,500);

        });

    }
}
export default DisplayFft;
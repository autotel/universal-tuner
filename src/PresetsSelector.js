// displays a list of frequencies, which produce a sine when clicked

import { Component, Div, Textarea, P, UploadField, TextField, H2 } from "./scaffolding/GraphicElements";
import FrequencyList from "./FrequencyList";
import sclToFrequency from "./scl-to-frequency";

class Button extends Div {
    constructor(){
        super();
        this.domElement.classList.add("button");
    }
}

const lowestFrequency = 60;
const highestFrequency = 10000;

/** @param {string} string */
const stringToFrequencyList = (string) => {
    let parts = string.match(/(\d+)[- ](TET|EDO|OVERTONES)@?([\d.]+)?/i);
    let tones = parseInt(parts[1]);
    let divisionType = (parts[2]+"").toUpperCase();
    let referenceFrequency = parseFloat(parts[3]) || 440;

    let ret = [];

    switch (divisionType){ 
        case "EDO":
        case "TET":{
            let baseFrequency = referenceFrequency;
            while (baseFrequency > lowestFrequency){
                baseFrequency /= 2;
            }
            let currentFrequency = baseFrequency
            for(let index=0; currentFrequency < highestFrequency; index++){
                currentFrequency = baseFrequency * Math.pow(2,index/tones);
                ret.push(currentFrequency);
            }
            break;
        }
        case "OVERTONES":{
            let currentFrequency = referenceFrequency
            for(let index=0; currentFrequency < highestFrequency; index++){
                currentFrequency = referenceFrequency * index/tones;
                ret.push(currentFrequency);
            }
            ret.sort();
            break;
        }
    }
    return ret;

}
/**
 * @typedef {Object} TuningPresetObject
 * @property {string} TuningPresetObject.name
 * @property {() => number[]} get
 */

/** @type {Array<TuningPresetObject>} */
const presetTunings = [
    {
        name:"2 overtones@110",
        //Not tested:
        get:()=>stringToFrequencyList("2 overtones@110"),
    },
    {
        name:"3 overtones@110",
        //Not tested:
        get:()=>stringToFrequencyList("3 overtones@110"),
    },
    {
        name:"4 overtones@110",
        //Not tested:
        get:()=>stringToFrequencyList("4 overtones@110"),
    },
    {
        name:"5 overtones@110",
        //Not tested:
        get:()=>stringToFrequencyList("5 overtones@110"),
    },
    {
        name:"6 overtones@110",
        //Not tested:
        get:()=>stringToFrequencyList("6 overtones@110"),
    },
    {
        name:"7 overtones@110",
        //Not tested:
        get:()=>stringToFrequencyList("7 overtones@110"),
    },
    {
        name:"overtones 2,3,4",
        get:()=>{
            return [...new Set([
                ...stringToFrequencyList("2 overtones@110"),
                ...stringToFrequencyList("3 overtones@110"),
                ...stringToFrequencyList("4 overtones@110"),
            ])];
        }
    },
    {
        name:"I don't know what to call this",
        //Not tested:
        get:()=>{
            let ret = [...new Set([
                ...stringToFrequencyList("2 overtones@110"),
                ...stringToFrequencyList("3 overtones@110"),
                ...stringToFrequencyList("4 overtones@110"),
            ].filter((fq)=>fq<=220))];
            ret.push( ... ret.map((f)=>f*2) );
            ret.push( ... ret.map((f)=>f*4) );
            ret.push( ... ret.map((f)=>f*8) );
            ret.push( ... ret.map((f)=>f*16) );
            ret.push( ... ret.map((f)=>f*32) );
            return ret;
        }
    },
    
    {
        name:"22-EDO",
        //Not tested:
        get:function(){
            let ret = [];
            let base = 20;
            for(let index=0; index<128; index++){
                ret.push(base * Math.pow(2,index/22));
            }
            return ret;
        }
    },
    {
        name:"12-TET@440: 12 tones equal temperament",
        //Not tested:
        get:()=>stringToFrequencyList("12-TET@440"),
    },
    {
        name:"Ptolemy's 12 tones",
        //Not tested:
        get:()=>{
            let ret = [];
            let baseFrequency = 440;
            let ratios = [
                1/1,
                16/15,
                9/8,
                6/5,
                5/4 ,
                4/3,
                45/32,
                3/2,
                8/5,
                5/3 ,
                9/5,
                15/8
            ];
            let tones = ratios.length;
            while (baseFrequency > lowestFrequency){
                baseFrequency /= 2;
            }
            let currentFrequency = baseFrequency
            for(let index=0; currentFrequency < highestFrequency; index++){
                currentFrequency = baseFrequency 
                    * ratios [index%tones] 
                    * Math.pow(2, Math.floor(index/tones) );
                ret.push(currentFrequency);
            }
            return ret;
        },
    },
    {
        name:"24-TET@440: 24 tones equal temperament",
        //Not tested:
        get:()=>stringToFrequencyList("24-TET@440"),
    },
    {
        name:"5-TET@440: 5 tones equal temperament",
        //Not tested:
        get:()=>stringToFrequencyList("5-TET@440"),
    },
    {
        name:"8-TET@440: 8 tones equal temperament",
        //Not tested:
        get:()=>stringToFrequencyList("8-TET@440"),
    }
    //todo: centaur http://www.anaphoria.com/centaur.html
    //and http://www.anaphoria.com/journal.html
]

class PresetSelectionButton extends Div {
    /** 
     * @param {Object} things
     * @param {FrequencyList} things.frequencyList
     * @param {TuningPresetObject} things.presetTuning
     */
    constructor({frequencyList,presetTuning}){
        super();
        this.domElement.classList.add("button");
        let hidden = false;
        this.highlighted = false;
        let label = new P();
        let frequency=0;
        this.add(label);
        this.hide = () => {
            if(!hidden){
                hidden=true;
                this.domElement.classList.add("hidden");
            }
        }
        this.show = () => {
            if(hidden){
                this.domElement.classList.remove("hidden");
            }
        }

        label.set("text",presetTuning.name);
        
            
        this.press = () => {
            frequencyList.setFrequencies(presetTuning.get());
        }
        this.release = () => {
        }

        this.domElement.addEventListener('mousedown',()=>this.press());
        this.domElement.addEventListener('mouseup',()=>this.release());
        
    }
}



class PresetSelector extends Div{
    /** 
     * @param {Object} things
     * @param {FrequencyList} things.frequencyList
     */
    constructor ({frequencyList}){
        super();
        const {readString,tuningToFrequencies} = sclToFrequency;
        //create Html elements
        let title = new H2({text:"Choose a preset scale"});
        this.add(title);
        let presetButtons = presetTunings.map(
            (presetTuning)=>new PresetSelectionButton({presetTuning,frequencyList})
        );
        presetButtons.forEach((pb)=>this.add(pb));

    }
}
export default PresetSelector;
import Model from "./scaffolding/Model";


 /** 
 * @typedef {Object} FrequencyListProperties
 * @property {Array<number>} FrequencyListProperties.frequencies
 * @property {number|false} FrequencyListProperties.selected
 * @property {number|false} FrequencyListProperties.selectedIndex
 */

/**
 * @class FrequencyList
 * @property {FrequencyListProperties} FrequencyList.settings
 */
class FrequencyList extends Model{
    constructor(){
        /**
         * @type {FrequencyListProperties} 
         */
        const settings = {
            frequencies:[],
            selected:false,
            selectedIndex:false,
        }
        super(settings);

        this.setFrequencies = (frequencies)=>{
            this.set({frequencies});
        }
        this.addFrequency = (frequency)=>{
            settings.frequencies.push(frequency);
            this.set({
                frequencies:settings.frequencies
            });
        }

        this.setFrequenciesFromCsv = (csvString)=>{
            const frequencies = csvString.split(/,\s*/g);
            this.set({frequencies});
        }

        this.getFrequenciesInCsv = () => {
            return settings.frequencies.join(', ');
        }

        this.selectFrequency = (frequency) => {
            let frequencyIndex = -1;
            if(frequency!==false){
                frequencyIndex = settings.frequencies.indexOf(frequency);
            }

            if(frequencyIndex<0){
                this.set({
                    selected:false,
                    frequencyIndex:false,
                });
            }else{
                this.set({
                    selected:frequency,
                    frequencyIndex:frequencyIndex,
                });
            }
        }

        this.beforeUpdate((changes)=>{
            if(changes.frequencies){
                changes.frequencies=changes.frequencies.sort((a,b)=>parseFloat(a)-parseFloat(b));
                const testSet = [];
                changes.frequencies=changes.frequencies.filter((frequency)=>{
                    if(isNaN(frequency)) return false;
                    if(testSet.includes(frequency)) return false;
                    testSet.push(frequency);
                    return true;
                });
            }
        });
    }
}

export default FrequencyList;
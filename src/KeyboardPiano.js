import FrequencyList from "./FrequencyList";
import mobileLog from "./model-dom/utils/mobile-log";
import Model from "./scaffolding/Model";

/**
 * @typedef {Object} KeyPosDef
 * @property {number} KeyPosDef.number //key number
 * @property {number} KeyPosDef.row //key row number
 * @property {number} KeyPosDef.col //key number within the row
 */
/**
 * @type {Object<string,KeyPosDef>}
 */
const keyMatrix = {
    "1": { number:0 ,row: 0, col: 0 },
    "2": { number:1 ,row: 0, col: 1 },
    "3": { number:2 ,row: 0, col: 2 },
    "4": { number:3 ,row: 0, col: 3 },
    "5": { number:4 ,row: 0, col: 4 },
    "6": { number:5 ,row: 0, col: 5 },
    "7": { number:6 ,row: 0, col: 6 },
    "8": { number:7 ,row: 0, col: 7 },
    "9": { number:8 ,row: 0, col: 8 },
    "0": { number:9 ,row: 0, col: 9 },
    "q": { number:10 ,row: 1, col: 0 },
    "w": { number:11 ,row: 1, col: 1 },
    "e": { number:12 ,row: 1, col: 2 },
    "r": { number:13 ,row: 1, col: 3 },
    "t": { number:14 ,row: 1, col: 4 },
    "y": { number:15 ,row: 1, col: 5 },
    "u": { number:16 ,row: 1, col: 6 },
    "i": { number:17 ,row: 1, col: 7 },
    "o": { number:18 ,row: 1, col: 8 },
    "p": { number:19 ,row: 1, col: 9 },
    "a": { number:20 ,row: 2, col: 0 },
    "s": { number:21 ,row: 2, col: 1 },
    "d": { number:22 ,row: 2, col: 2 },
    "f": { number:23 ,row: 2, col: 3 },
    "g": { number:24 ,row: 2, col: 4 },
    "h": { number:25 ,row: 2, col: 5 },
    "j": { number:26 ,row: 2, col: 6 },
    "k": { number:27 ,row: 2, col: 7 },
    "l": { number:28 ,row: 2, col: 8 },
    "ñ": { number:28 ,row: 2, col: 8 },
    ",": { number:29 ,row: 2, col: 10 },
    "z": { number:30 ,row: 3, col: 0 },
    "x": { number:31 ,row: 3, col: 1 },
    "c": { number:32 ,row: 3, col: 2 },
    "v": { number:33 ,row: 3, col: 3 },
    "b": { number:34 ,row: 3, col: 4 },
    "n": { number:35 ,row: 3, col: 5 },
    "m": { number:36 ,row: 3, col: 6 },
}
class KeyboardPiano extends Model {
    /**
     * @param {Object} things
     * @param {FrequencyList} things.frequencyList
     */
    constructor({ frequencyList }) {
        const settings = {
            willPlay:document.hasFocus()
        }
        super(settings);
        let maxColumn = 9;

        this.setIsomorphicKeyboardWidth = (to) => {
            maxColumn = to;
        }

        const pressCallbacks = [];
        const releaseCallbacks = [];
        this.onPress = (callback) => {
            pressCallbacks.push(callback);
        }
        this.onRelease = (callback) => {
            releaseCallbacks.push(callback);
        }

        /**
         * translate keyboard position to tone as isomorphic keyboard
         * 
         * @param {KeyPosDef} pos */
        const posToTone=(pos)=>{
            return (pos.col + pos.row * maxColumn) % frequencyList.settings.frequencies.length;
        }

        /** @param {KeyPosDef} pos */
        const handleKeyPress = (pos) => {
            let tone=posToTone(pos);
            pressCallbacks.forEach((callback)=>callback(tone));
        }
        /** @param {KeyPosDef} pos */
        const handleKeyRelease = (pos) => {
            let tone=posToTone(pos);
            releaseCallbacks.forEach((callback)=>callback(tone));
        }
        const evaluateFocus = ()=>{
            let tagName=document.activeElement.tagName.toLowerCase();
            this.set({
                //not perfect way to determine whether it will play or not.
                willPlay: tagName!=="input"
                    && tagName!=="textarea"
            })
            console.log(document.activeElement.tagName);
        }

        //events
        // frequencyList.onUpdate((changes) => {
        //     console.log(changes);
        //     if (changes.frequencies) {
        //     }
        // });

        const pressedKeys = [];

        document.onkeypress = function (e) {
            evaluateFocus();
            e = e || window.event;
            if(pressedKeys.includes(e.key)) return;
            pressedKeys.push(e.key);
            // use e.keyCode
            if (settings.willPlay) {
                let pos = keyMatrix[e.key];
                if(pos) handleKeyPress(pos);
            }
        };
        document.onkeyup = function (e) {
            evaluateFocus();
            e = e || window.event;
            pressedKeys.splice(pressedKeys.indexOf(e.key),1);
            // use e.keyCode
            if (settings.willPlay) {
                let pos = keyMatrix[e.key];
                if(pos) handleKeyRelease(pos);
            }
        };
        document.addEventListener("mousedown",evaluateFocus);
        document.addEventListener("mouseup",evaluateFocus);
        document.addEventListener("touchstart",evaluateFocus);
        document.addEventListener("touchend",evaluateFocus);
    }

}

export default KeyboardPiano;
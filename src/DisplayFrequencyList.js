// displays a list of frequencies, which produce a sine when clicked

import Synthesizer from "./Synthesizer";
import { Component, Div, Textarea, P } from "./model-dom/GuiComponents/DOMElements";
import FrequencyList from "./FrequencyList";
import KeyboardPiano from "./KeyboardPiano";

class FrequencyButton extends Div {
    /** 
     * @param {Object} things
     * @param {Synthesizer} things.synthesizer
     * @param {FrequencyList} things.frequencyList
     */
    constructor({synthesizer,frequencyList}){
        super();
        this.domElement.classList.add("button","frequency","key");
        let hidden = false;
        this.highlighted = false;
        let label = new P();
        let frequency=0;
        this.add(label);
        this.hide = () => {
            if(!hidden){
                hidden=true;
                this.domElement.classList.add("hidden");
            }
        }
        this.show = () => {
            if(hidden){
                this.domElement.classList.remove("hidden");
            }
        }
        this.highlight = () => {
            if(!this.highlighted){
                this.domElement.classList.add("highlighted");
                this.highlighted=true;
            }
        }
        this.removeHighlight = () => {
            if(this.highlighted){
                this.domElement.classList.remove("highlighted");
                this.highlighted=false;
            }
        }
        this.set=(settings)=>{
            this.show();
            frequency=settings.frequency;

            label.attributes.text = (Math.round(frequency * 100 ) / 100)+"Hz";
            label.update();

        }

        frequencyList.onUpdate((changes)=>{
            if(changes.selected!==undefined){
                if(frequency===changes.selected){
                    this.highlight();
                }else{
                    this.removeHighlight();
                }
            }
        });

        const toggleSelected = () => {
            if(this.highlighted){
                frequencyList.selectFrequency(false);
            }else{
                frequencyList.selectFrequency(frequency);
            }
        }

        this.press = () => {
            synthesizer.startTone(frequency);
            // toggleSelected();
            frequencyList.selectFrequency(frequency);
        }
        this.release = () => {
            synthesizer.stopTone(frequency);
        }

        this.domElement.addEventListener('mousedown',()=>this.press());
        this.domElement.addEventListener('mouseup',()=>this.release());
        
    }
}

class DisplayFrequencyList extends Div{
    /** 
     * @param {Object} things
     * @param {Synthesizer} things.synthesizer
     * @param {FrequencyList} things.frequencyList
     * @param {KeyboardPiano|undefined} things.keyboard;
     */
    constructor ({synthesizer,frequencyList,keyboard}){
        super();

        //create Html elements
        let textarea = new Textarea({style:"min-height:200px; height:calc(100vh - 40.5vw - 200px); width:calc(100% - 15px)"});
        let frequencyButtonsContainer = new Div({class:"frequency-buttons-container"});

        this.add(frequencyButtonsContainer,textarea);

        //private scope
        const list=frequencyList;
        /** @type {Array<FrequencyButton>} */
        const frequencyButtons = [];

        const drawFrequencyButton = (number,properties) => {
            let frequencyButton = false;
            if(!frequencyButtons[number]){
                frequencyButtons[number] = new FrequencyButton({synthesizer,frequencyList});
                frequencyButtonsContainer.add(frequencyButtons[number]);
            }
            frequencyButtons[number].set(properties);
        }
        const hideFrequencyButtons = (from, to=frequencyButtons.length) => {
            for(let index=from; index<to; index++) frequencyButtons[index].hide();
        }
        
        //create event triggers
        const updateFrequencyButtons = () => {
            const frequencies = list.settings.frequencies;
            
            frequencies.forEach((frequency,number)=>{
                drawFrequencyButton(number,{
                    frequency
                });
            });
            hideFrequencyButtons(frequencies.length);
        }

        let preventTextareaUpdate = false;

        const updateTextarea = () => {
            if(preventTextareaUpdate) return;
            textarea.domElement.value=list.getFrequenciesInCsv();
        }

        textarea.domElement.addEventListener("mousedown",()=>{
            preventTextareaUpdate=true;
        });
        textarea.domElement.addEventListener('focus',(ev)=>{
            preventTextareaUpdate=true;
        });
        textarea.domElement.addEventListener('focusout',(ev)=>{
            preventTextareaUpdate=false;
        });

        textarea.onInput(()=>{
            list.setFrequenciesFromCsv(textarea.domElement.value);
        });

        list.onUpdate((changes)=>{
            if(changes.frequencies){
                updateFrequencyButtons();
                updateTextarea();
            }
        });

        keyboard.onPress((toneNumber)=>{
            let button=frequencyButtons[toneNumber];
            if(button){
                button.press();
            }
        });
        keyboard.onRelease((toneNumber)=>{
            let button=frequencyButtons[toneNumber];
            if(button){
                button.release();
            }
        });

        //triggers and initial state
        list.setFrequenciesFromCsv(textarea.domElement.innerHTML);



    }
}
export default DisplayFrequencyList;
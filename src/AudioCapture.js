/**
 * creates an input node and captures audio
 */
class AudioCapture{
    /** 
     * @param {Object} things
     * @param {AudioContext} things.audioContext
     */
    constructor({audioContext}){
        /** @type {boolean} */
        this.isReady=false;
        /** @type {MediaStreamAudioSourceNode|false} */
        this.audioSource=false;

        const constraints = { audio: true };

        const readyListeners = [];

        this.onReady=(readyListener)=>{
            if(this.isReady) return readyListener();
            readyListeners.push(readyListener);
        }

        const handleReady = () => readyListeners.forEach((rl)=>rl());

        this.onReady(()=>this.isReady=true);

        navigator.mediaDevices.getUserMedia(constraints)
        .then((stream)=>{
            this.audioSource = audioContext.createMediaStreamSource(stream);
            handleReady();
        })
        .catch(console.error);
    }
}
export default AudioCapture;
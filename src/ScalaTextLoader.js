// displays a list of frequencies, which produce a sine when clicked

import Synthesizer from "./Synthesizer";
import { Component, Div, Textarea, P, UploadField, TextField, H2 } from "./scaffolding/GraphicElements";
import FrequencyList from "./FrequencyList";
import sclToFrequency from "./scl-to-frequency";
class ScalaTextLoader extends Div{
    /** 
     * @param {Object} things
     * @param {FrequencyList} things.frequencyList
     */
    constructor ({frequencyList}){
        super();
        const {readString,tuningToFrequencies} = sclToFrequency;
        //create Html elements
        let title = new H2({text:"upload scala (.scl) file"});
        let uploadField = new UploadField();
        uploadField.domElement.accept="*.scl";
        let baseFrequencyField = new TextField({
            id:"scl-frequency-base",placeholder:"frequency zero"
        });

        this.add(title,baseFrequencyField,uploadField);
        
        const textReceived=(text)=>{
            console.log("parse as scl",text);
            let newFrequencies = readString(
                text,
                0,
                parseInt(baseFrequencyField.domElement.value)||60
            );
            console.log(newFrequencies);
            frequencyList.setFrequencies(newFrequencies);
        }

        uploadField.onInput(()=>{
            if (!window.FileReader) {
                alert('Your browser is not supported');
                return false;
            }

            let input = uploadField.domElement;
            // Create a reader object
            let reader = new FileReader();

            if (input.files.length) {
                var textFile = input.files[0];
                reader.readAsText(textFile);
                reader.onload=()=>{
                    textReceived(reader.result);
                };
            } else {
                alert('Please upload a file before continuing')
            } 
        });
    }
}
export default ScalaTextLoader;